# Zephyr - fork of KDE Breeze style

![zephyr screenshot](screenshot.png)

Zephyr is pretty much one-to-one fork of Breeze: https://invent.kde.org/plasma/breeze

I renamed Breeze to Zephyr, so I can keep my original Breeze installed.

Everything has been renamed! And some files have been removed.

This is basically my testing ground for changes for Breeze and my own variation of it.

**I did not make the whole thing!** Again, this is a fork of Breeze, but renamed
and with some of my own modifications added.

The first commit ( https://codeberg.org/akselmo/Zephyr/commit/a8279d2f7031ecc4a065f0baf47310e84f131222 )
has nothing else differing from the commit https://invent.kde.org/plasma/breeze/-/tree/ca9ccd11d7b59f705ebabc4866814467a1f48725
except the renaming.

# How to

1. Clone this repo
2. Make sure you have all the stuff you need for building Breeze installed.
	- No, I do not remember what the packages are lol. Feel free to make an issue about this
3. Run `./install.sh`
4. Done! Now you should see Zephyr in system settings -> appearance.
5. Sometimes you may need to relog or restart your PC to get the themes working.
6. Some settings (especially with window deco) may require you to switch to other theme and then back to Zephyr
	- Or just reboot.

Then you can also copy the Zephyr plasma style in `~/.local/share/plasma/desktoptheme/`. But it should also be on the KDE store, just search for Zephyr.

Looks the best with Revontuli colorscheme! https://codeberg.org/akselmo/Revontuli

If you want to remove zephyr, run `./uninstall.sh`

# Rounded corners

Check `zephyrmetrics.h` file for frame radius etc. settings. You have to recompile to change the radius.

Zephyr uses 4, default Breeze radius is 3.

# Qt Quick apps

Some changes wont show in QtQuick apps. This is due to Qt and uhh i dont even know how deep it goes.
I heard rumors that it will be fixed in Qt6 but who knows.

# Qt6 version

Will make this when my system updates from 5 to 6.

# I want more stuff!

Check out Klassy, it may be better for you :) https://github.com/paulmcauley/klassy