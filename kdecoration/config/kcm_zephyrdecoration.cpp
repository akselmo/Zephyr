#include "zephyrconfigwidget.h"
#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(Zephyr::ConfigWidget, "kcm_zephyrdecoration.json")

#include "kcm_zephyrdecoration.moc"
