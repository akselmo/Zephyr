/*
 * SPDX-FileCopyrightText: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#include "zephyrstyleplugin.h"
#include "zephyrstyle.h"

#include <QApplication>

namespace Zephyr
{
//_________________________________________________
QStyle *StylePlugin::create(const QString &key)
{
    if (key.toLower() == QStringLiteral("zephyr")) {
        return new Style;
    }
    return nullptr;
}

//_________________________________________________
QStringList StylePlugin::keys() const
{
    return QStringList(QStringLiteral("Zephyr"));
}

}
