//////////////////////////////////////////////////////////////////////////////
// zephyranimationconfigitem.h
// animation configuration item
// -------------------
//
// SPDX-FileCopyrightText: 2010 Hugo Pereira Da Costa <hugo.pereira@free.fr>
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

#include <QAbstractScrollArea>
#include <QApplication>
#include <QIcon>

#include <KCMultiDialog>
#include <KLocalizedString>
#include <KPluginMetaData>

//__________________________________________
int main(int argc, char *argv[])
{
    KLocalizedString::setApplicationDomain("zephyr_style_config");

    QApplication app(argc, argv);
    app.setApplicationName(i18n("Zephyr Settings"));
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("zephyr-settings")));

    KCMultiDialog dialog;
    dialog.setWindowTitle(i18n("Zephyr Settings"));
    dialog.addModule(KPluginMetaData(QStringLiteral("plasma/kcms/systemsettings_qwidgets/zephyrstyleconfig")));
    dialog.addModule(KPluginMetaData(QStringLiteral("plasma/kcms/zephyr/kcm_zephyrdecoration")));
    dialog.show();

    foreach (auto child, dialog.findChildren<QAbstractScrollArea *>()) {
        child->adjustSize();
        child->viewport()->adjustSize();
    }

    return app.exec();
}
